\documentclass[
11pt, 
a4paper, 
onecolumn, 
oneside, 
notitlepage,
]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{csquotes}

\usepackage{graphicx}

\usepackage[
nochapters,
beramono,
eulermath,
]{classicthesis}

\usepackage{varioref}

\usepackage{amsmath}

\usepackage[authordate]{biblatex-chicago}
\DeclareFieldFormat[article]{title}{#1}
\DeclareFieldFormat[article]{author}{#1}
\addbibresource{ref.bib}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{\normalfont\spacedallcaps{COMP188 Text Summarisation Survey}}

\author{\textbf{Richard Rui Gao} \\
Macquarie University, NSW, Australia \\
Student: richard.gao@students.mq.edu.au \\
Mentor: diego.molla-aliod@mq.edu.au
}

\date{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
  \maketitle
  \hrule
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \begin{abstract}
The purpose of this paper will be to provide insight into the research and development of automatic text extraction and summarisation. Automated text summarisation is the automatic extraction and reduction of one or more source texts, while preserving the original context. Automatic summarisation of texts has been growing in necessity, attributed to the increasing amount of online information. Text summarisation based on manual approaches can be inefficient and costly, particularly with regards to large documents of text and multiple sources. In recent years, with the introduction of machine learning algorithms, automated summarisation has been addressed from multiple perspectives. This survey intends to investigate the principal techniques of automated summarisation with specific focus on extractive approaches.
  \end{abstract}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \section{Introduction}
With the recent advent of the World Wide Web, and its ability to contain and spread an exponentially growing amount of documents globally, information overload is becoming an increasingly important problem. Since information on data networks is still mostly contained in text format, as in hypertext, subtitles, etc., information can become detrimental to users due to the vast and redundant quantity available. Thus, tools that provide quick insights into single or multiple documents can become necessary in parsing new information. Manual text summarisation is inefficient and a time-consuming task, especially with the vast amount of documents needed to be parsed and the swift speed with which important information travels globally. Automated text summarisation systems can be used in multiple situations; summarising letters or scientific papers in a research context, or summarising key information from news networks in order to create a cohesive content regarding one topic. The Google search engine\footnote{https://www.google.com} summarises the most relevant web pages in regards to the query parsed into the system, and can also answer queries by using query summarisation techniques (i.e. inputting a general but explicit question will return an explicit answer from the engine). Thus, the Google search engine is a popular tool that employs the use of extractive summarisation to quickly answer queries. Newsblaster\footnote{http://www1.cs.columbia.edu/nlp/projects.cgi} is another example of an automatic text summariser. Newsblaster is a text summariser system developed by Colombia University that “automatically collects, clusters, categorises, and summarises news from several sites on the web (CNN, Reuters, Fox News, etc.) on a daily basis”. Aramaki et al. (2009) proposed the development of TEXT2TABLE, a medical text summarisation system that converts a medical text into table structure by extracting medical events and date times from a text.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Key Words and Definition}
A summary is loosely defined as “a text that is produced from one or more texts, that conveys important information in the original text(s), and is no longer than half of the original text(s) and usually significantly less than that” (Radev et al. 2002), where text is referred to as any type of information such as speech, multimedia documents, hypertext, etc. From this simple definition, we can gather that a summary must preserve important information, be brief, and is produced from one or more documents. Other researchers have provided extended definitions for summary, but the core concept of a summary is to reduce reading time and should reflect the context and key ideas of the source document(s).
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Types of Summary}
Researchers in automated text summarisation have identified many types of summary (Sp\"{a}rck Jones 1999; Hovy and Lin 1999). Indicative summaries provide the context of the source material without giving any specific content. Informative summaries provide a shortened version of the source content. Radev et al. (2002) defines common terms used in summarisation: \textit{extracts} are summaries created by reusing portions of the original text; \textit{abstraction} is the procedure of producing significant information by re-generating the extracted content; \textit{fusion} combines extracted parts into a coherent sentence; \textit{compression} reduces the source text by cutting out insignificant portions of the source material. This survey focuses on extractive text summarisation methods.
\par
Extractive summaries are created by extracting key portions of the original text based on statistical analysis of each sentence or paragraph with surface level features such as word/phrase frequency, location, or cue words. Essentially, key portions are sentences or paragraphs that contain the most frequent or most favourably positioned content in the source. Extractive summaries are, therefore, conceptually simple and easy to implement, but avoid deep text understanding – learning syntax and semantics.
\par
Gupta and Lehal (2010) explain that problems with extractive summarisation techniques are that key segments that are extracted tend to be lengthier than average and parts of the segments that are redundant or inessential are also included. Pure extraction can also lead to problems with overall cohesion and coherence of the summary as extracts are de-contextualized. This leads to inaccurate representation of source information. For example, pronouns and temporal expressions lose their referents when extracted out of context. This is especially apparent when extracting from multiple sources.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Early History}
Automatic summarisation methods have been researched for almost the last half century with early experimentation occurring from the late 1950s. The first few experimentations with automated summarisation suggested that it may have been feasible, though tedious (Baxendale 1958; Luhn 1958; Edmundson 1969).
\par
Luhn (1958) proposed a system that used surface-level features based on the frequency of particular words in an article. Words from an article are stemmed to their root forms. A list of content words is compiled sorted by decreasing frequency where the index ranked the significance of each word. The list excludes words that are the most frequent, such as prepositions or determiners. These excluded words are part of a predefined list of stop words. A significance factor is then created for each sentence based on the number of occurrences of significant words within the sentence and the linear distance between the words within the sentence. The sentences are ranked in order of their significance factor and the top selected sentences are used to form the summary.
\par
Although Luhn's work is the most cited, Baxendale's research and Luhn's research would both be extended in Edmundson's paper. While Edmundson does not refer to Baxendale's research, Edmundson's research includes similar features that are proposed by Baxendale. Baxendale's work described the usage of the position of portions in the source text to find the most useful and significant sentences in the document. Edmundson used Luhn's systems and their chosen features. Most modern extraction summarisation methods are based off of Luhn's and Edmundson's systems. The major three features used by Edmundson are:
\begin{enumerate}
\item 
\textbf{Cue Method}: A cue dictionary is compiled that contains cue words that are categorised as bonus, stigma, and null words. Cue words are words from the source documentations. These words are categorised based on their selection ratio in the original texts; selection ratio is the frequency of a word in extractor selected sentences to the frequency of the word in the text(s). Bonus words are above a specified threshold, stigma words are below another chosen threshold, and null words are between these thresholds. Null words must also be above a selected dispersion. Bonus words are weighted positively and stigma words are weighted negatively. The method then finds the most relevant sentences by ordering the sentences by the total sum of the weights from the bonus and stigma words that occur within them. Null words are weighted neutrally and do not affect the sentence weighting. 
\item
\textbf{Title Method}: A title glossary is created that contains all non-null words that appear in the title, subtitle, headings and subheadings of the text(s). Sentences are weighted based on the amount of title glossary words that are used in them. Content words from titles are assigned heavier weights relatively prime to words from headings.
\item
\textbf{Location Method}: This method assigns positive weights to sentences if they contain words in a preselected Heading dictionary. This dictionary contains words that appear in the headings of documents such as “Introduction” and “Conclusion”. As well, the first and last sentences of a paragraph and sentences in the first and last paragraphs in a text are assigned positive weights.  
\end{enumerate}
Edmundson found that using the Cue-Title-Location weighting systems in conjunction yielded the highest mean accuracy in his experimentations. His evaluation of accuracy was based on correlation between human-made manual summary extracts and the experiments' automatic extracts.
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \section{Approaches}
    \subsection{Term Frequency-Inverse Document Frequency (TF-IDF)}
This is a statistical approach to determine the significance of a word within the text(s). This method is based off Luhn's assumption that the frequency of word occurrences in an article reflects the word's significance within the article. The flaw with the original assumption is that words that occur very often are usually pronouns or determiners and are insignificant. Traditionally, researchers like Luhn used a predefined list of stop words in order to ignore the useless words. Determining which words should be stop words is not simple; stop words could become significant words depending on the domain and context. Notably, Sp\"{a}rck Jones (1972) observed that words that occur in too many documents are less useful to characterise individual documents due to their popularity.
\par
Thus, in order to solve this dilemma, inverse document frequency was proposed by Sp\"{a}rck Jones (1972) where, used in conjunction with term frequency, could determine the importance of a word relative to the source text. Originally used for information retrieval and text mining, the TF-IDF value increases proportionally to the frequency of a word in an article, but is offset by the frequency of a word in the total collection of articles. The collection of articles, or background corpus, serves as an indication of how often a word could appear in another generic and arbitrary text. Typically, the texts in the corpus are of the same genre, in order to add stop words that are common to the specific domain. Thus TF-IDF is calculated as:
\begin{equation}
  \text{TF-IDF}_{w} = F(w)\times\log\frac{D}{D(w)}
\end{equation}
Where \textit{w} is a word, \textit{F(w)} is frequency of a word, \textit{D} is the total documents, and \textit{D(w)} is the number of documents containing a word.
\par
Descriptive and potentially useful words are those that appear frequently in one document, but are uncommon in other documents, so their TF-IDF score will be quite relative to their frequency in the source text. Stop words appear in all documents so their IDF score will be close to zero. Thus stop words will have near zero TF-IDF weighting. Since this system is simple and computationally cheap, it is popularly used in modern weighting systems in conjunction with other weights, or incorporated into other weighting systems.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{WordNet}
WordNet is a large lexical database of English words. Developed like a thesaurus, WordNet groups words together based on their meanings. These groups (synsets) are interlinked by their lexical relations.
\par
Pal and Saha (2013) developed an extractive summarisation system based on the Simplified Lesk algorithm and WordNet. The Simplified Lesk algorithm is an algorithm adapted from Lesk (1986) which determines the meaning or sense of a word as it is used in a sentence (as certain English words have multiple meanings). This problem is otherwise known as word-sense disambiguation (WSD). The Simplified Lesk algorithm compares the dictionary definition of a word to another word and finds the intersection or overlap if it occurs. If overlap occurs, that definition is chosen.
\par
Pal and Saha's paper uses this algorithm as a weighting system, in conjunction with stop word removal. Sentences are weighted based on the number of intersections that occur between dictionary definitions (glosses) of every non-stop word in the sentence and the source text. The glosses are extracted using WordNet. The benefit of using this method is the method's independence of parameters that traditional methods depend upon (i.e. predetermined thresholds and format of the source text).
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Clustering}
Clustering or a cluster-based approach is used to decrease the amount of information by categorising similarly structured and themed data. This method summarises a text by reducing the document into its key sentences. Cluster-based approaches depend upon the use of other summarisation techniques to extract the key texts.
\par
Sarkar (2009) and Deshpande and Lobo (2013) developed a cluster based automatic text summarisation system. Similar sentences are grouped together into sentence clusters and the sentences are weighted using text summarisation algorithms. The scores of each sentence are summed for each cluster and the clusters are ordered from highest to lowest weights. Finally, the highest weighted sentence in each sentence cluster is selected; the number of sentences to be selected is based on the amount of the sentence clusters.
\par
Clusters are created based on the cosine similarity metric. Sentences are represented by a vector of weights, such as the TF-IDF score of each word in the sentence. Sarkar uses a uni-gram matching similarity measure instead, to overcome problems with sparse feature vectors. That is, short sentences that share only one frequent word will have high cosine similarity value.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Graph Theory}
Graph theory can be applied to source texts as a step for extractive summarisation. The graph-theoretic representation of the article depicts the structure and the relationship between sentences of the article. Sentences in the document are represented as each node and the edges between the nodes are similarity relations between the sentences. Similarity relations depend on the features. For example, an edge is added if the cosine similarity measure of them is above a certain threshold. The resultant graph depicts the different topics and themes in the source document, with each sub-graph representing the topics. 
\par
There are multiple methods of finding the most significant sentences using graph theory. The simplest method is representing the nodes with the highest cardinality (number of edges) as the most significant sentences in each sub-graph. Text summarisation then occurs by choosing the most significant sentences from each sub-graph. Graph theory can be desirable for visualisation of document structures and multi-document similarity relations. TextRank algorithm is an example of a graph-based ranking model that is proposed in Mihalcea and Tarau (2004) which can be applied for automated text summarisation.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Machine Learning}
Pioneered by Kupiec et al. (1995), their machine learning approaches to text summarisation used a Bayesian classifier to combine multiple features from a wide range of research papers. Kupiec et al. intended to determine which features should be used in weighting the sentences and how they should be weighted relative to each feature. Their experimentation used features based on Paice (1990), as well as upper-case word feature and sentence length cut-off (to avoid including short sentences below a certain length). Using the Naïve Bayes classifier for learning, a corpus of technical documents with manual abstracts (created by professional abstractors) was used. Each sentence in the documents were manually mapped to whether it matched with the sentences in the manual abstract. These mapping included “exact match”, “incomplete partial match”, “match of joined sentences”, etc. Finally, the extracted sentences from the automatic summarisation method were evaluated against the mappings. Kupiec et al. found that a combination of location, cue, and sentence length features gave the most accurate summarisation.
\par
Alternatively, Lin and Hovy (1997) experimented with machine learning methods to study the significance of a single feature. They determined the relationship between sentence ordinal positions in a text to its selection for a summary. Using a news-wire corpus containing technical documents on computer hardware and their respective abstracts, Lin and Hovy found the optimal parameters for the position feature method. 
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \section{Evaluation}
Automatic evaluation of the quality of a summary is an important task in its ability to efficiently and quickly return evaluation results. Manual methods of evaluation are undesirable as they are costly, in terms of time and resources. That is to say, manual tasks are slower than machine-oriented tasks, so the removal of human-dependent elements can increase efficiency and reliability. Thus the research and development of automatic evaluation systems for summaries is key to the research and development of automatic text summarisation itself. Typical methods require intrinsic evaluation which focus on human judgements of the quality of the given summary. A standard method in evaluation is comparing a given summary to that of a gold-standard summary.
\par
The problem with evaluating the quality of a summary is the subjectivity that is involved. Especially with humans involved, extractive summaries can vary wildly for the source text. Even with straightforward articles such as news reports, summaries extracted are typically different. Thus a gold-standard summary, even with human involvement, can become challenging to implement.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Recall-Oriented Understudy for Gisting Evaluation (ROUGE)}
ROUGE\footnote{http://www.berouge.com/Pages/default.aspx} is the most commonly used metric for extractive summarisation methods in research and development due to its computationally cheap and fast system. This system requires a gold-standard summary, or model, extracted from the given text and will evaluate the given summary with the respective model. Derived from the machine translation evaluation algorithm Bilingual Evaluation Understudy (BLEU)\footnote{http://www.langtech.co.uk/us/language-translation-industry-glossary/167-use-of-bleu-score.html}, ROUGE uses n-gram matching as well as numerous parameters (e.g. word stemming, n-gram size, etc.) to compare the given summary with its model. N-gram matching involves converting sentences into sets of n-grams as vectors to allow an efficient comparison of sentences. ROUGE uses n-gram matching to calculate the precision and recall of the given summary against the model summary, as well as the F-measure (the weighted average). These are given as:
\begin{equation}
  \text{Precision} = \frac{c}{c+w}
\end{equation}

\begin{equation}
  \text{Recall} = \frac{c}{c+m}
\end{equation}

\begin{equation}
  F = 2\cdot\frac{\text{Precision}\cdot\text{Recall}}{\text{Precision}+\text{Recall}}
\end{equation}
Where \textit{F} is the F-measure, \textit{c} is the number of elements that match, \textit{w} is the number of elements in the system that do not appear in the model, and \textit{m} is the number of elements in the model that do not appear in the system.
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \section{Conclusions}
Currently, automated text summarisation has become a growing research. With the exponential increase of information and the potential for information overload in the public sphere, automatic text summarisation has become a necessary field of study. While extractive summarisation techniques resemble the techniques that originated in the late 1950s, numerous advances in computer processing speed and natural language processing has allowed the optimisation and improvement of these original systems. The availability of large text corpora through the internet has allowed the exploration of supervised methods for summarisation, such as machine learning approaches. Machine learning, in particular, has given way for automatic experimentation; more efficient compared to manual feature selections and experimentations. Even though both abstractive and extractive summarisation methods have been attempted, abstractive summaries require more computational power and time, and are difficult to replicate or extend into broader domains (i.e. different topics require the algorithm to be refitted).
\par
Extractive summarisation techniques have been explored thoroughly in many variations, over the last half century. But without deeper text understanding from natural language processing, the extracted summary will be flawed. Multiple document summarisation can generate summaries that are unbalanced. Cohesion and semantics can become lost in the extracted segments when concatenated together. The weights of each feature is the most important decision in deciding the quality of the final summary.
\par
The current challenge that faces automated summarisation is the ability to summarise multiple content from different sources, both structural and textual (i.e. databases, websites, emails, etc., all together). A good automatic text summarisation system should produce the summary in the least time and least redundancy. Automatic evaluation metric systems have been developed and used to evaluate the ability of a text summarisation system. But these systems require further work. Nenkova and McKeown (2011) states that ROUGE rarely detects significant differences between automated text summarisation systems, and can show that systems may perform better or equal to their human counterpart. This can be misleading as manual methods show that humans outperform text summarisation systems. Thus, evaluation systems should be researched more in order to reduce these problems.
\par
Summarisation can be developed further for future uses. One potential domain is the growth of social networking systems. Summaries can be useful for data analysis or as general user information in navigating a network, by summarising activities and chat logs. Another domain that has yet to be fully explored is update summarisation. Update summarisation refers to a summary that will update over time while following a stream of news live. As summarisation methods and technology advance, automated text summarisation systems and their evaluation systems should start to improve in abilities.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Research Proposal}
From reviewing the given techniques in extractive summarisation, the method that shall be incorporated for future study within the course will be machine learning techniques with the addition of feature selections, primarily TF-IDF. The proposed research will focus on feature analysis and optimisation. From the limited knowledge available on the countless and varied methods that have been studied on extractive summarisation, we can gather that TF-IDF is one of the more simpler and efficient methods developed. In order to branch out from the research already available on TF-IDF implementation, we shall use machine learning techniques in addition to the TF-IDF feature to optimise and analyse this single feature. Certainly, most state-of-the-art techniques rely on the use of multiple techniques built upon each other; clustering required the use of features (i.e. TF-IDF) in addition to its own clustering techniques to find the significant sentences; some implementations of graph theory can use features to create edges between each node. Machine learning, although relatively recent, has already proved useful in analysis research, and so can be useful in furthering the research on features used for extractive summarisation. The final goal is to implement these techniques into other areas of summarisation, such as query summarisation, update summarisation, and multi-document summarisation. 
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \nocite{*}
  \printbibliography
\end{document}