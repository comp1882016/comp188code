"""
Support Vector Machine Regression

Uses support vector machine as a machine learning method, to learn the
weights of each word from a source text (training set).

Author: Richard Rui Gao <richard.gao@students.mq.edu.au>
Mentor: Diego Molla-Aliod <diego.molla-aliod@mq.edu.au>
"""

from sklearn import svm, feature_extraction

import subprocess
import re

import numpy as np
import json
import csv
import random

from nltk import word_tokenize, sent_tokenize

import evaluation


########################################################################################################################
#                                                 Main Functions
########################################################################################################################


def summarise(trainingCase, devTestCase, n=3, kernel='rbf', js="BioASQ-trainingDataset4b.json",
              cs="rouge_4b.csv", seed=1234):
    """Uses the question IDs to find a summary (the 'n' sentences with highest predicted SU4).

    >>> summarise([0], [1]) # doctest: +ELLIPSIS
    {1: ['In addition, there seem to be ...', '...', '...']}

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be returned for each question ID.

    kernel: string, optional
        Type of kernel to be used for learning the data sets.
        Can be ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    summary: Dict{int: List[string]}
        Summary of each tested question ID.
    """
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))

    svr, cv = _train_svr(trainingCase, kernel, cs, seed)

    # Create array of sentences from snippets per question
    snippetsPerId = []
    for qid in questionids:
        totalSnippets = []
        q = questions[int(qid)]
        snippets = q['snippets']
        for snippet in snippets:
            sentences = sent_tokenize(snippet['text'])
            for sentence in sentences:
                token = word_tokenize(sentence)
                alnumSent = [word for word in token if word.isalnum()]
                if alnumSent:
                    totalSnippets.append(sentence)
        snippetsPerId.append(totalSnippets)

    # Summarise question/snippets
    topNList = []
    for qid in devTestCase:
        summariseThis = snippetsPerId[qid]
        summariseThis = np.asarray(summariseThis)

        cvSentTest = cv.transform(summariseThis)

        result = svr.predict(cvSentTest)

        topNIdx = np.argsort(result)[::-1][:n]
        topNSents = summariseThis[topNIdx]
        topNList.append(list(topNSents))
    summary = dict(zip(devTestCase, topNList))
    return summary


def compare_cue_rouge(n=3, kernel='rbf', upper=0.3, lower=0.1, dispersion=1, bonusWeight=1, stigmaWeight=1,
                      js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234, rougePath=".."):
    """Compare weight of words in linearRegression to cue dictionary words using ROUGE.

    >>> compare_cue_rouge(kernel="linear") # doctest: +SKIP
    Cue Dictionary F-score: 0.16962
    Support Vector Regression F-score: 0.16885
    >>> compare_cue_rouge(kernel="rbf") # doctest: +SKIP
    Cue Dictionary F-score: 0.16962
    Support Vector Regression F-score: 0.17986
    >>> compare_cue_rouge(kernel="poly") # doctest: +SKIP
    Cue Dictionary F-score: 0.16962
    Support Vector Regression F-score: 0.16704

    Parameters
    ----------
    n: int, optional
        Number of sentences to be used for each question ID.

    kernel: string, optional
        Type of kernel to be used for learning the data sets.
        Can be ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable.

    upper: float, optional
        Threshold for determining bonus words; between [0, 1].

    lower: float, optional
        Threshold for determining stigma words; between [0, 1].

    dispersion: float, optional
        Dispersion threshold for determining null words; positive float.

    bonusWeight: float, optional
        The weight to be used for bonus words; typically positive int.

    stigmaWeight: float, optional
        The weight to be used for stigma words; typically positive int. Sign is flipped.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    rougePath: string, optional
        Path where ROUGE is located.
    """
    # Open json file for questionids
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    # Create training and devTest cases
    # Note trainingCase contains both validation and training <- built-in cross-validation
    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.8))
    devTestCase = [qid for qid in questionids if qid not in trainingCase]

    # Initialise ridgeRegressionCV and cueDict
    peers = [evaluation.cue_dict_html(trainingCase, devTestCase, n, upper, lower, dispersion, bonusWeight,
                                      stigmaWeight, js, cs)[1],
             evaluation.sup_vec_reg_html(trainingCase, devTestCase, n, kernel, js, cs, seed)[1]]

    models = evaluation.model_html(devTestCase, js)[0]

    evaluation.generate_xml(devTestCase, peers, models)

    # Calculate ROUGE scores and read output
    with subprocess.Popen("perl " + rougePath + "/ROUGE-1.5.5.pl -e " + rougePath + "/data -a -x -2 4 -u settings.xml",
                          shell=True, stdout=subprocess.PIPE, universal_newlines=True).stdout as stream:
        lines = stream.read()

    cueDictScore = float(re.search("(?<=cueDict\sROUGE-SU4\sAverage_F:\s)(\d.\d{5})", lines).group(0))
    supVecRegScore = float(re.search("(?<=supVecReg\sROUGE-SU4\sAverage_F:\s)(\d.\d{5})", lines).group(0))

    # Print ROUGE average F-scores
    print('Cue Dictionary F-score: {0}'.format(cueDictScore))
    print('Support Vector Regression F-score: {0}'.format(supVecRegScore))


########################################################################################################################
#                                              Private Helper Functions
########################################################################################################################


def _train_svr(trainingCase, kernel='rbf', cs="rouge_4b.csv", seed=1234):
    """Train SVR and countVectorizer for use in other modules.

    >>> a, b = _train_svr([0])
    >>> type(a), type(b)
    (<class 'sklearn.svm.classes.SVR'>, <class 'sklearn.feature_extraction.text.CountVectorizer'>)

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    kernel: string, optional
        Type of kernel to be used for learning the data sets.
        Can be ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    svr: LinearSVR
        A fitted LinearSVR function.

    cv: CountVectorizer
        A fitted CountVectorizer function.
    """
    svr = svm.SVR(kernel=kernel)
    cv = feature_extraction.text.CountVectorizer(token_pattern='\\b\\w+\\b')

    # Load csv file for sentence extraction and oracle SU4 scores
    # Load json file for questionids
    with open(cs, encoding='utf-8') as f:
        scores = [row for row in csv.reader(f)]

    # Get total SU4 scores per snippet sentence from csv file
    # Get snippet sentences from csv file
    header = scores[0]
    SU4Index = header.index('SU4')
    qidIndex = header.index('qid')
    textIndex = header.index('sentence text')
    trainScores = [float(row[SU4Index]) for row in scores[1:] for qid in trainingCase if int(row[qidIndex]) == qid]
    trainSentences = [row[textIndex] for row in scores[1:] for qid in trainingCase if int(row[qidIndex]) == qid]

    # Shuffle data
    random.seed(seed)
    random.shuffle(trainScores)
    random.seed(seed)
    random.shuffle(trainSentences)

    # Use CountVectorizer to count words from training data
    cvSentTrain = cv.fit_transform(trainSentences)

    # Fit datasets
    svr.fit(cvSentTrain, trainScores)

    return svr, cv


if __name__ == "__main__":
    import doctest
    doctest.testmod()
