"""
Linear Regression

Uses linear regression as a machine learning method, to learn the weights
of each word from a source text (training set).
The summarise() method will summarise a given list of question IDs by
predicting the SU4 scores of each sentence from a question, and returning
the top 'n' sentences for each question.
This will take several minutes to run.

Author: Richard Rui Gao <richard.gao@students.mq.edu.au>
Mentor: Diego Molla-Aliod <diego.molla-aliod@mq.edu.au>
"""

from sklearn import linear_model, feature_extraction

import subprocess
import re

import numpy as np
import json
import csv
import random

from matplotlib import pyplot as plt

from nltk import word_tokenize, sent_tokenize

import cueDictionary
import evaluation


########################################################################################################################
#                                                 Main Functions
########################################################################################################################


def summarise(trainingCase, devTestCase, n=3, js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234):
    """Uses the question ids to find a summary (the N sentences with highest predicted SU4).

    >>> summarise([0], [1]) # doctest: +ELLIPSIS
    {1: ['It therefore appears that the LFS phenotype ...', '...', '...']}

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used as training data.

    devTestCase: List[int]
        List of question IDs to be summarised.

    n: int, optional
        Number of sentences to be returned per question ID.

    js: string, optional
        JSON file to be used for question IDs.

    cs: string, optional
        CSV file to be used for SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    summary: Dict{int: List[string]}
        Summary of each tested question ID.
    """
    # Load json file for questionids
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))

    lr, cv = train_lr(trainingCase, cs, seed)

    # Create array of sentences from snippets per question
    snippetsPerId = []
    for qid in questionids:
        totalSnippets = []
        q = questions[int(qid)]
        snippets = q['snippets']
        for snippet in snippets:
            sentences = sent_tokenize(snippet['text'])
            for sentence in sentences:
                token = word_tokenize(sentence)
                alnumSent = [word for word in token if word.isalnum()]
                if alnumSent:
                    totalSnippets.append(sentence)
        snippetsPerId.append(totalSnippets)
    snippetsPerId = np.asarray(snippetsPerId)

    # Summarise question/snippets
    topNList = []
    for qid in devTestCase:
        summariseThis = snippetsPerId[qid]
        summariseThis = np.asarray(summariseThis)

        cvSentTest = cv.transform(summariseThis)

        result = lr.predict(cvSentTest)

        topNIdx = np.argsort(result)[::-1][:n]
        topNSents = summariseThis[topNIdx]
        topNList.append(list(topNSents))
    summary = dict(zip(devTestCase, topNList))
    return summary


def compare_cue(n=3, upperBound=0.3, lowerBound=0.1, dispersionLimit=1,
                js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234, bias=0.01):
    """Compare weight of words in linearRegression to cue dictionary words.

    >>> compare_cue() # doctest: +SKIP
    ----------------------------
    Percent of words match their coefficients:

    bonus: 50.318%
    stigma: 49.116%
    null: 2.129%
    ----------------------------
    Number of words:

    linearRegression | cueDict
    bonus:      2989 | bonus:      2989
    stigma:     1018 | stigma:     1018
    null:       3617 | null:       3617
    ----------------------------
    Possible weight thresholds?:

    -9.516 >= bonus  >= 8.449
    -4.641 >= stigma >= 4.204
    -6.218 >= null   >= 6.550
    ----------------------------
    Average weights:

    bonus: 0.062
    stigma: -0.010
    null: -0.002

    Parameters
    ----------
    n: int, optional
        Number of sentences to be used for each training ID.

    upperBound: float, optional
        Threshold for determining bonus words; between [0, 1].

    lowerBound: float, optional
        Threshold for determining stigma words; between [0, 1].

    dispersionLimit: float, optional
        Dispersion threshold for determining null words; positive int.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    bias: float, optional
        How much tolerance should be allowed.
    """
    # Open json file for questionids
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    # Randomise questionids for use in cueDict
    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.6))

    # Initialise linearRegression and cueDict
    wordsAndWeights = list(find_weights(trainingCase, cs, seed).items())
    cueDict = cueDictionary.generate_cue(trainingCase, n, upperBound, lowerBound, dispersionLimit, js, cs)

    # Place each cueDict keys' items into their respective variables
    bonus = cueDict['bonus']
    stigma = cueDict['stigma']
    null = cueDict['null']

    # Separate weights from linearRegression for their matching words
    # in their respective cueDict keys
    bonusUnion = [tup[1] for tup in wordsAndWeights if tup[0] in bonus]
    stigmaUnion = [tup[1] for tup in wordsAndWeights if tup[0] in stigma]
    nullUnion = [tup[1] for tup in wordsAndWeights if tup[0] in null]

    # Count how many words from their unions match their coefficients
    # Bonus: weight is positive
    bonusCount = len([weight for weight in bonusUnion if weight > 0 + bias])
    bonusPercent = (bonusCount / len(bonusUnion)) * 100
    # Stigma: weight is negative
    stigmaCount = len([weight for weight in stigmaUnion if weight < 0 - bias])
    stigmaPercent = (stigmaCount / len(stigmaUnion)) * 100
    # Null: weight is neutral
    nullCount = len([weight for weight in nullUnion if 0 + bias >= weight >= 0 - bias])
    nullPercent = (nullCount / len(nullUnion)) * 100

    # Find possible thresholds for weights
    bonusThreshold = [min(bonusUnion), max(bonusUnion)]
    stigmaThreshold = [min(stigmaUnion), max(stigmaUnion)]
    nullThreshold = [min(nullUnion), max(nullUnion)]

    # Average weights
    bonusAverage = np.average(bonusUnion)
    stigmaAverage = np.average(stigmaUnion)
    nullAverage = np.average(nullUnion)

    # Print results
    print('----------------------------')
    print('Percent of words match their coefficients:\n')
    print('bonus: {0:.3f}%'.format(bonusPercent))
    print('stigma: {0:.3f}%'.format(stigmaPercent))
    print('null: {0:.3f}%'.format(nullPercent))

    print('----------------------------')
    print('Number of words:\n')
    print('{0} | {1:10}'.format('linearRegression', 'cueDict'))
    print('bonus: {0:9} | bonus: {1:9}'.format(len(bonusUnion), len(bonus)))
    print('stigma: {0:8} | stigma: {1:8}'.format(len(stigmaUnion), len(stigma)))
    print('null: {0:10} | null: {1:10}'.format(len(nullUnion), len(null)))

    print('----------------------------')
    print('Possible weight thresholds?:\n')
    print('{0:.3f} >= bonus  >= {1:.3f}'.format(bonusThreshold[0], bonusThreshold[1]))
    print('{0:.3f} >= stigma >= {1:.3f}'.format(stigmaThreshold[0], stigmaThreshold[1]))
    print('{0:.3f} >= null   >= {1:.3f}'.format(nullThreshold[0], nullThreshold[1]))

    print('----------------------------')
    print('Average weights:\n')
    print('bonus: {0:.3f}'.format(bonusAverage))
    print('stigma: {0:.3f}'.format(stigmaAverage))
    print('null: {0:.3f}'.format(nullAverage))

    # Create a histogram plot for each word group
    # Bonus
    plt.figure(1)
    plt.hist(bonusUnion, bins=100, alpha=0.75)
    plt.title('Bonus')
    plt.xlabel('Weight')
    plt.ylabel('Amount')
    plt.xticks(np.arange(-5, 6, 0.5))
    plt.axhline(0, color='k')
    plt.axvline(0, color='k')
    plt.grid(True)
    # Stigma
    plt.figure(2)
    plt.hist(stigmaUnion, bins=100, alpha=0.75)
    plt.title('Stigma')
    plt.xlabel('Weight')
    plt.ylabel('Amount')
    plt.xticks(np.arange(-5, 6, 0.5))
    plt.axhline(0, color='k')
    plt.axvline(0, color='k')
    plt.grid(True)
    # Null
    plt.figure(3)
    plt.hist(nullUnion, bins=100, alpha=0.75)
    plt.title('Null')
    plt.xlabel('Weight')
    plt.ylabel('Amount')
    plt.xticks(np.arange(-5, 6, 0.5))
    plt.axhline(0, color='k')
    plt.axvline(0, color='k')
    plt.grid(True)

    plt.show()


def compare_cue_rouge(n=3, upper=0.3, lower=0.1, dispersion=1, bonusWeight=1, stigmaWeight=1,
                      js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234, rougePath=".."):
    """Compare weight of words in linearRegression to cue dictionary words using ROUGE.

    >>> compare_cue_rouge() # doctest: +SKIP
    Cue Dictionary F-score: 0.16962
    Linear Regression F-score: 0.15065

    Parameters
    ----------
    n: int, optional
        Number of sentences to be used for each training ID.

    upper: float, optional
        Threshold for determining bonus words; between [0, 1].

    lower: float, optional
        Threshold for determining stigma words; between [0, 1].

    dispersion: float, optional
        Dispersion threshold for determining null words; positive int.

    bonusWeight: float, optional
        The weight to be used for bonus words; typically positive int.

    stigmaWeight: float, optional
        The weight to be used for stigma words; typically positive int. Sign is flipped.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    rougePath: string, optional
        Path where ROUGE is located.
    """
    # Open json file for questionids
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    # Create training and devTest cases
    # Note trainingCase contains both validation and training <- built-in cross-validation
    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.8))
    devTestCase = [qid for qid in questionids if qid not in trainingCase]

    # Initialise ridgeRegressionCV and cueDict
    peers = [evaluation.cue_dict_html(trainingCase, devTestCase, n, upper, lower, dispersion, bonusWeight,
                                      stigmaWeight, js, cs)[1],
             evaluation.lin_reg_html(trainingCase, devTestCase, n, js, cs, seed)[1]]
    models = evaluation.model_html(devTestCase, js)[0]

    evaluation.generate_xml(devTestCase, peers, models)

    # Calculate ROUGE scores and read output
    with subprocess.Popen("perl " + rougePath + "/ROUGE-1.5.5.pl -e " + rougePath + "/data -a -x -2 4 -u settings.xml",
                          shell=True, stdout=subprocess.PIPE, universal_newlines=True).stdout as stream:
        lines = stream.read()

    cueDictScore = float(re.search("(?<=cueDict\sROUGE-SU4\sAverage_F:\s)(\d.\d{5})", lines).group(0))
    linRegScore = float(re.search("(?<=linReg\sROUGE-SU4\sAverage_F:\s)(\d.\d{5})", lines).group(0))

    # Print ROUGE average F-scores
    print('Cue Dictionary F-score: {0}'.format(cueDictScore))
    print('Linear Regression F-score: {0}'.format(linRegScore))


########################################################################################################################
#                                                Helper Functions
########################################################################################################################


def train_lr(trainingCase, cs="rouge_4b.csv", seed=1234):
    """Train linear regression and count vectorizer for use in other modules.

    >>> a, b = train_lr([0])
    >>> type(a), type(b)
    (<class 'sklearn.linear_model.base.LinearRegression'>, <class 'sklearn.feature_extraction.text.CountVectorizer'>)

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used as training data.

    cs: string, optional
        CSV file to be used for SU4 scores.

    seed: any, optional
        Seed for use in randomising.

    Returns
    -------
    lr: LinearRegression
        A fitted LinearRegression function.

    cv: CountVectorizer
        A fitted CountVectorizer function.
    """
    # Initialise LinearRegression and CountVectorizer
    lr = linear_model.LinearRegression()
    cv = feature_extraction.text.CountVectorizer(token_pattern='\\b\\w+\\b')

    # Load csv file for sentence extraction and oracle SU4 scores
    # Load json file for questionids
    with open(cs, encoding='utf-8') as f:
        scores = [row for row in csv.reader(f)]

    # Get total SU4 scores per snippet sentence from csv file
    # Get snippet sentences from csv file
    header = scores[0]
    SU4Index = header.index('SU4')
    qidIndex = header.index('qid')
    textIndex = header.index('sentence text')
    trainScores = [float(row[SU4Index]) for row in scores[1:] for qid in trainingCase if int(row[qidIndex]) == qid]
    trainSentences = [row[textIndex] for row in scores[1:] for qid in trainingCase if int(row[qidIndex]) == qid]

    # Shuffle data
    random.seed(seed)
    random.shuffle(trainScores)
    random.seed(seed)
    random.shuffle(trainSentences)

    # Use CountVectorizer to count words from training data
    cvSentTrain = cv.fit_transform(trainSentences)

    # Fit datasets
    lr.fit(cvSentTrain, trainScores)

    return lr, cv


def find_weights(trainingCase, cs="rouge_4b.csv", seed=1234):
    """Use sklearn's linear regression module to estimate the linear combination of task2
    that best predicts the SU4 of a sentence. Use the training set to fit the linear regression model.
    Returns weights of each word and the word dictionary mapping

    >>> dictObject = find_weights([0])
    >>> dictObject['strabismus']
    -0.039276829531147403

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used as training data.

    cs: string, optional
        CSV file to be used for SU4 scores.

    seed: any, optional
        Seed for use in randomising.

    Returns
    -------
    wordDict: Dict{string: float}
        A dictionary of words and their learned weights.
    """
    # Initialise LinearRegression and CountVectorizer
    lr = linear_model.LinearRegression()
    cv = feature_extraction.text.CountVectorizer(token_pattern='\\b\\w+\\b')

    # Load csv file for sentence extraction and oracle SU4 scores
    # Load json file for questionids
    with open(cs, encoding='utf-8') as f:
        scores = [row for row in csv.reader(f)]

    # Get total SU4 scores per snippet sentence from csv file
    # Get snippet sentences from csv file
    header = scores[0]
    SU4Index = header.index('SU4')
    qidIndex = header.index('qid')
    textIndex = header.index('sentence text')
    trainScores = [float(row[SU4Index]) for row in scores[1:] for qid in trainingCase if int(row[qidIndex]) == qid]
    trainSentences = [row[textIndex] for row in scores[1:] for qid in trainingCase if int(row[qidIndex]) == qid]

    # Shuffle data
    random.seed(seed)
    random.shuffle(trainScores)
    random.seed(seed)
    random.shuffle(trainSentences)

    # Use CountVectorizer to count words from training data
    cvSentTrain = cv.fit_transform(trainSentences)

    # Fit datasets
    lr.fit(cvSentTrain, trainScores)

    # Find mapping of words to numbers
    # And convert numbers back to words
    vocab = list(cv.vocabulary_.items())
    sortedVocabTuples = sorted(vocab, key=lambda tup: tup[1])
    sortedVocabKeys = [tup[0] for tup in sortedVocabTuples]

    # Create dict mapping each word to its weight
    wordsAndWeights = dict(zip(sortedVocabKeys, lr.coef_))
    return wordsAndWeights


if __name__ == "__main__":
    import doctest
    doctest.testmod()
