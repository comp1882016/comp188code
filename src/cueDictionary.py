"""
Cue Dictionary

Based on Edmundson (1969), categorises the source text into bonus, stigma,
and null words based on satisfaction of certain thresholds.
The summarise() method summarises a given list of question IDs by weighting
all the sentences from each question ID, and finding the top 'n' sentences
for each question.

Author: Richard Rui Gao <richard.gao@students.mq.edu.au>
Mentor: Diego Molla-Aliod <diego.molla-aliod@mq.edu.au>
"""

import json
import csv
import random

from nltk import word_tokenize, FreqDist, sent_tokenize


########################################################################################################################
#                                                 Main Functions
########################################################################################################################


def summarise(trainingIds, devTestIds, n=3, upperBound=0.3, lowerBound=0.1, dispersionLimit=1,
              bonusWeight=1, stigmaWeight=1, js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234):
    """Return dictionary of questionids and top n sentences based on bonus and stigma weights.

    >>> summarise([0,1], [2,3]) # doctest: +ELLIPSIS
    {2: ['Alteplase can be given to patients ...', '...', '...'], 3: ['In addition ...', '...', '...']}

    Parameters
    ----------
    trainingIds: List[int]
        List of question IDs to be used for training.

    devTestIds: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each training ID.

    upperBound: float, optional
        Threshold for determining bonus words; between [0, 1].

    lowerBound: float, optional
        Threshold for determining stigma words; between [0, 1].

    dispersionLimit: float, optional
        Dispersion threshold for determining null words; positive int.

    bonusWeight: float, optional
        The weight to be used for bonus words; typically positive int.

    stigmaWeight: float, optional
        The weight to be used for stigma words; typically positive int. Sign is flipped.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    summary: Dict{int: List[string]}
        Summary of each tested question ID.
    """
    cue = generate_cue(trainingIds, n, upperBound, lowerBound, dispersionLimit, js, cs)

    with open(js) as f:
        data = json.load(f)
    questions = data['questions']

    weightedQIds = []
    for qid in devTestIds:
        sentWeights = []
        q = questions[qid]
        snippets = q['snippets']

        random.seed(seed)
        random.shuffle(snippets)

        for snippet in snippets:
            sentences = sent_tokenize(snippet['text'])
            for sent in sentences:
                token = word_tokenize(sent)
                alnumToken = [word.lower() for word in token if word.isalnum()]
                weight = 0
                for word in alnumToken:
                    if word in cue['bonus']:
                        weight += bonusWeight
                    elif word in cue['stigma']:
                        weight -= stigmaWeight
                sentWeights.append((sent, weight))
        weightedQIds.append(sentWeights)

    topNList = []
    for qid in weightedQIds:
        sortWeight = sorted(qid, key=lambda tup: tup[1], reverse=True)
        topN = sortWeight[:n]
        topNSents = [sent[0] for sent in topN]
        topNList.append(topNSents)
    summary = dict(zip(devTestIds, topNList))
    return summary


########################################################################################################################
#                                                Helper Functions
########################################################################################################################


def generate_cue(questionids, n=3, upperBound=0.3, lowerBound=0.1, dispersionLimit=1,
                 js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv"):
    """Return a cue dictionary based on questionids and number of sentences.

    >>> cues = generate_cue([0,1,2,3,4,5,6,7,8,9,10])
    >>> sorted(list(cues.keys()))
    ['bonus', 'null', 'stigma']
    >>> sorted(cues['null']) # doctest: +ELLIPSIS
    ['actomyosin', 'administered', 'administration', 'after', 'also', ...
    >>> print(len(cues['bonus']), len(cues['null']), len(cues['stigma']))
    197 105 31

    Parameters
    ----------
    questionids: List[int]
        List of question IDs to be used.

    n: int, optional
        Number of sentences to be used for each question ID.

    upperBound: float, optional
        Threshold for determining bonus words; between [0, 1].

    lowerBound: float, optional
        Threshold for determining stigma words; between [0, 1].

    dispersionLimit: float, optional
        Dispersion threshold for determining null words; positive int.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    Returns
    -------
    cueDict: Dict{string: List[string]}
        Mapping of given words to bonus, null, and stigma categories.
    """
    # Open json and csv files as questions and scores respectively
    with open(js) as file1, open(cs, encoding='utf-8') as file2:
        data = json.load(file1)
        scores = [row for row in csv.reader(file2)]
    questions = data['questions']

    # Create a dictionary of question ids and top n sentences based on CSV SU4 scores.
    idTopNTuple = []
    for qid in questionids:
        rows = []
        header = scores[0]
        qIdIndex = header.index('qid')
        SU4Index = header.index('SU4')
        sentIndex = header.index('sentence text')
        for row in scores[1:]:
            if qid == int(row[qIdIndex]):
                sentSU4 = (row[SU4Index], row[sentIndex])
                rows.append(sentSU4)
        rows.sort(key=lambda tup: tup[0], reverse=True)
        topNSU4 = rows[:n]
        topNSent = [sent[1] for sent in topNSU4]
        idTopNTuple.append((qid, topNSent))
    topN = dict(idTopNTuple)

    # Create a string of sentences separated by whitespace for ideal sentences based on topN dictionary.
    # Snippet sentences based on snippet text in json.
    snip = ""
    ideal = ""
    for qid in questionids:
        for i in range(n):
            if i < len(topN[qid]):
                ideal += topN[qid][i] + " "
            else:
                break
        q = questions[qid]
        snippets = q['snippets']
        for snippet in snippets:
            snip += snippet['text'] + " "

    # Tokenise and find the frequency distribution of each word in ideal string.
    tokenIdeal = word_tokenize(ideal)
    alphaTokenIdeal = [word.lower() for word in tokenIdeal if word.isalnum()]
    fdistIdeal = FreqDist(alphaTokenIdeal)

    # Tokenise and find the frequency distribution of each word in snip string.
    tokenSnip = word_tokenize(snip)
    alphaTokenSnip = [word.lower() for word in tokenSnip if word.isalnum()]
    fdistSnip = FreqDist(alphaTokenSnip)

    # Calculate dispersion of words; no of documents a word occurs in.
    fullAlphaListTokens = []
    for qid in questionids:
        for i in range(n):
            if i < len(topN[qid]):
                listTokens = word_tokenize(topN[qid][i])
            else:
                break
            alphaListTokens = [word.lower() for word in listTokens if word.isalnum()]
            fullAlphaListTokens += set(alphaListTokens)
    fdistDispersion = FreqDist(fullAlphaListTokens)

    # Calculate selection ratio and dispersion of each word and combine them into a list of triples.
    words = []
    selRatio = []
    dispersion = []
    for word in fdistIdeal:
        if word in fdistSnip and word in fdistDispersion:
            words.append(word)
            selRatio.append(fdistIdeal[word] / fdistSnip[word])
            dispersion.append(fdistDispersion[word])
    cueList = list(zip(words, selRatio, dispersion))

    # Create list for each key; list is bounded by condition from thresholds
    bonus = [k for k, v, d in cueList if v > upperBound]
    null = [k for k, v, d in cueList if lowerBound <= v <= upperBound and d >= dispersionLimit]
    stigma = [k for k, v, d in cueList if v < lowerBound]

    # Combine lists into one dictionary
    cueDict = {"bonus": bonus, "null": null, "stigma": stigma}
    return cueDict


if __name__ == "__main__":
    import doctest
    doctest.testmod()
