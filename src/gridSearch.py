"""
Grid Search

Used for cueDictionary.py module, gridSearch.py will find the optimal thresholds
based on the SU4 (skip four-gram) scores. It will brute force calculate all the
possible thresholds between 0 to 1 and retain the thresholds with the highest SU4 score.
This will take several minutes to run, especially for higher iterations.

Author: Richard Rui Gao <richard.gao@students.mq.edu.au>
Mentor: Diego Molla-Aliod <diego.molla-aliod@mq.edu.au>
"""

import subprocess
import re
import itertools

import numpy as np
import json
import random

import evaluation


########################################################################################################################
#                                                 Main Functions
########################################################################################################################


def find_thresholds(lower=0, higher=1, noIter=10, rougePath="..", js="BioASQ-trainingDataset4b.json", seed=1234):
    """Fine-tune thresholds. Uses train set to compute the cues, devTest set to test ROUGE scores.
    Program will do exhaustive search of possible values of each threshold, record values that produce best ROUGE.

    >>> find_thresholds() # doctest: +SKIP +ELLIPSIS
    ----------------------------
    2.22% done
    Best ROUGE score: 0.1703
    Current ROUGE score: 0.1703
    Best thresholds: [0.10000000000000001, 0.0]
    Current thresholds: [0.1, 0.0]
    ...

    Parameters
    ----------
    lower: float, optional
        The lowest threshold to start testing.

    higher: float, optional
        The highest threshold to finish testing.

    noIter: int, optional
        Number of iterations to test between [0,1].

    rougePath: string, optional
        Location of ROUGE folder.

    js: string, optional
        The JSON file to be used for question IDs.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    array: List[float]
        List containing optimal thresholds; [bonusThreshold, stigmaThreshold].
    """
    # Initialise the json and csv files for external use.
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']

    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    # Generate random training, and devTest sets.
    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.8))
    devTestCase = [qid for qid in questionids if qid not in trainingCase]

    # Initialise variables to find optimal thresholds using previous weights.
    threshold = np.linspace(lower, higher, noIter, endpoint=False)
    score = 0.
    thresholdArray = []
    count = 0

    # Counts total loops to calculate percentage of loops done.
    total = sum(range(noIter))

    # Generate models and XML file.
    peers = [evaluation.cue_dict_html(trainingCase, devTestCase)[1]]
    models = evaluation.model_html(devTestCase, js)[0]
    evaluation.generate_xml(devTestCase, peers, models)

    # Brute force method to find optimal thresholds.
    for upper, lower in itertools.product(threshold, repeat=2):
        # If upper <= lower then cueDictionary won't work
        # Skips checking this step to reduce computational time
        if upper > lower:
            evaluation.cue_dict_html(trainingCase, devTestCase, upper=upper, lower=lower)

            with subprocess.Popen("perl " + rougePath + "/ROUGE-1.5.5.pl -e " + rougePath +
                                  "/data -x -2 4 -u settings.xml cueDict", shell=True, stdout=subprocess.PIPE,
                                  universal_newlines=True).stdout as stream:
                lines = stream.read()
            # Regex to find ROUGE f-score
            newScore = float(re.search("(?<=F:\s)(\d.\d{5})", lines).group(0))

            if score < newScore:
                score = newScore
                thresholdArray = [upper, lower]
            count += 1

            print('----------------------------')
            print('{0:.2f}% done'.format((count / total) * 100))
            print('Best ROUGE score:', score)
            print('Current ROUGE score:', newScore)
            print('Best thresholds:', thresholdArray)
            print('Current thresholds: [{0}, {1}]'.format(upper, lower))
    print('----------------------------')
    print('Final thresholds:', thresholdArray)
    return thresholdArray


def find_weights(lower=1, higher=10, noIter=10, thresholdArray=(0.3, 0.1), rougePath="..",
                 js="BioASQ-trainingDataset4b.json", seed=1234):
    """Fine-tune thresholds. Uses train set to compute the cues, devTest set to test ROUGE scores.
    Program will do exhaustive search of possible values of each threshold, record values that produce best ROUGE.

    >>> find_weights() # doctest: +SKIP +ELLIPSIS
    ----------------------------
    1.0 % done
    Best ROUGE score: 0.16962
    Current ROUGE score: 0.16962
    Best weights: [1.0, 1.0]
    Current weights: [1.0, 1.0]
    ...

    Parameters
    ----------
    lower: float, optional
        The lowest threshold to start testing.

    higher: float, optional
        The highest threshold to finish testing.

    noIter: int, optional
        Number of iterations to test between [0,1].

    thresholdArray: tuple, optional
        The upper and lower threshold to be used for cueDictionary.

    rougePath: string, optional
        Location of ROUGE folder.

    js: string, optional
        The JSON file to be used for question IDs.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    array: List[float]
        List containing optimal weights; [bonusWeight, stigmaWeight].
    """
    # Initialise the json and csv files for external use.
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']

    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    # Generate random training, and devTest sets.
    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.8))
    devTestCase = [qid for qid in questionids if qid not in trainingCase]

    # Generate models and XML file.
    peers = [evaluation.cue_dict_html(trainingCase, devTestCase)[1]]
    models = evaluation.model_html(devTestCase, js)[0]
    evaluation.generate_xml(devTestCase, peers, models)

    # Initialise variables to find optimal weights based on default thresholds
    weight = np.linspace(lower, higher, noIter)
    weightScore = 0.
    weightArray = []
    count = 0

    for bonusWeight, stigmaWeight in itertools.product(weight, repeat=2):
        # If program first init, run full rougeGenerate, otherwise just create cueDict html files
        evaluation.cue_dict_html(trainingCase, devTestCase, 3, thresholdArray[0], thresholdArray[1], 1,
                                 bonusWeight, stigmaWeight, js)

        with subprocess.Popen("perl " + rougePath + "/ROUGE-1.5.5.pl -e " + rougePath +
                              "/data -x -2 4 -u settings.xml cueDict", shell=True, stdout=subprocess.PIPE,
                              universal_newlines=True).stdout as stream:
            lines = stream.read()
        # Regex to find ROUGE F-score
        weightNewScore = float(re.search("(?<=F:\s)(\d.\d{5})", lines).group(0))

        if weightScore < weightNewScore:
            weightScore = weightNewScore
            weightArray = [bonusWeight, stigmaWeight]
        count += 1

        print('----------------------------')
        print((count / (noIter ** 2)) * 100, '% done')
        print('Best ROUGE score:', weightScore)
        print('Current ROUGE score:', weightNewScore)
        print('Best weights:', weightArray)
        print('Current weights: [{0}, {1}]'.format(bonusWeight, stigmaWeight))
    print('----------------------------')
    print('Final weights:', weightArray)
    return weightArray


if __name__ == "__main__":
    import doctest
    doctest.testmod()
