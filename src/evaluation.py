"""
ROUGE Generation

Generates system and model files for use in ROUGE (see ROUGE documentation for more details).
Can be run directly without importing. It will calculate the ROUGE scores for all the
cueDictionary.py and lexicalDiversity.py modules as well as randomly selected sentences and
the CSV's (oracle) top 'n' sentences.
Requires system and model folder to be created before use.

Author: Richard Rui Gao <richard.gao@students.mq.edu.au>
Mentor: Diego Molla-Aliod <diego.molla-aliod@mq.edu.au>
"""

from xml.etree.ElementTree import Element, SubElement
from xml.etree import ElementTree
from xml.dom import minidom

import json
import csv
import random

from nltk import sent_tokenize

import lexicalDiversity
import cueDictionary
import linearRegression
import ridgeRegressionCV
import maximalMarginalRelevance
import supportVectorMachine


########################################################################################################################
#                                                 Main Functions
########################################################################################################################


def evaluate_all(n=3, upper=0.3, lower=0.1, dispersion=1, bonusWeight=1, stigmaWeight=1, alphas=(0, 1, 10), numCV=10,
                 lambda_=0.7, ngram=(1, 2), kernel='rbf', js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv",
                 seed=1234):
    """Initialises different data sets and creates peer and model HTML files from them.
    A settings.xml file is created using the HTML files.
    Requires systems and models folder to be created in the same directory.

    Parameters
    ----------
    n: int, optional
        Number of sentences to be used for each question ID.

    upper: float, optional
        Threshold for determining bonus words; between [0, 1].

    lower: float, optional
        Threshold for determining stigma words; between [0, 1].

    dispersion: float, optional
        Dispersion threshold for determining null words; positive float.

    bonusWeight: float, optional
        The weight to be used for bonus words; typically positive int.

    stigmaWeight: float, optional
        The weight to be used for stigma words; typically positive int. Sign is flipped.

    alphas: List[int], optional
        List of alphas to be tested.

    numCV: int, optional
        Number of folds used for cross validation.

    lambda_: float, optional
        Lambda value between [0, 1].

    ngram: Tuple(int, int)
        Tuple of lower and upper boundaries for range of n-values to be used in n-gram extraction.

    kernel: string, optional
        Type of kernel to be used for learning the data sets.
        Can be ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.
    """
    # Initialise the json and csv files for external use.
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']

    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    # Generate random training, and devTest sets.
    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.8))
    devTestCase = [qid for qid in questionids if qid not in trainingCase]

    # Save file names into peers and also generate the HTML files.
    peers = [rand_sel_html(devTestCase, n, js, seed)[1],
             lex_div_html(devTestCase, n, js)[1],
             cue_dict_html(trainingCase, devTestCase, n, upper, lower,
                           dispersion, bonusWeight, stigmaWeight, js, cs)[1],
             lin_reg_html(trainingCase, devTestCase, n, js, cs, seed)[1],
             ridge_cv_html(trainingCase, devTestCase, n, alphas, numCV, js, cs, seed)[1],
             max_mar_rel_html(trainingCase, devTestCase, n, lambda_, ngram, js, cs, seed)[1],
             sup_vec_reg_html(trainingCase, devTestCase, n, kernel, js, cs, seed)[1],
             oracle_html(devTestCase, n, cs)[1]]
    model = model_html(devTestCase, js)[0]

    # Generate settings.xml file.
    generate_xml(devTestCase, peers, model)


########################################################################################################################
#                                                Helper Functions
########################################################################################################################


def rand_sel_html(devTestCase, n=3, js="BioASQ-trainingDataset4b.json", seed=1234):
    """Modularised randSel HTML files for use in other Python files.

    >>> randSel, fileName = rand_sel_html([0])
    >>> randSel # doctest: +ELLIPSIS
    {0: ['Tarsal coalition is a distinct feature ...', '...', '...']}
    >>> fileName
    'randSel'

    Parameters
    ----------
    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    randSel: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'randSel'

    with open(js) as f:
        data = json.load(f)
    questions = data['questions']

    totalSentences = []
    idRandomNTuple = []
    random.seed(seed)
    for qid in devTestCase:
        q = questions[qid]
        snippets = q['snippets']
        snippetSentences = []
        for snippet in snippets:
            sentences = sent_tokenize(snippet['text'])
            for sent in sentences:
                snippetSentences.append(sent)
        totalSentences += snippetSentences
        random3Sentences = random.sample(snippetSentences,
                                         min(n,len(snippetSentences)))
        idRandomNTuple.append((qid, random3Sentences))

    randSel = dict(idRandomNTuple)

    generate_html(randSel, fileName)

    return randSel, fileName


def lex_div_html(devTestCase, n=3, js="BioASQ-trainingDataset4b.json"):
    """Modularised lexDiv HTML files for use in other Python files.

    >>> lexDiv, fileName = lex_div_html([0])
    >>> lexDiv # doctest: +ELLIPSIS
    {0: ['Muenke syndrome is characterized by coronal ...', '...', '...']}
    >>> fileName
    'lexDiv'

    Parameters
    ----------
    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    Returns
    -------
    lexDiv: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'lexDiv'

    with open(js) as f:
        data = json.load(f)
    questions = data['questions']

    idLexDivTuple = []
    for qid in devTestCase:
        fullListTokens = []
        q = questions[qid]
        snippets = q['snippets']
        for snippet in snippets:
            sentences = sent_tokenize(snippet['text'])
            for sent in sentences:
                fullListTokens.append(sent)
        summariseList = [sent[0] for sent in lexicalDiversity.summarise(fullListTokens, n)]
        idLexDivTuple.append((qid, summariseList))
    lexDiv = dict(idLexDivTuple)

    generate_html(lexDiv, fileName)

    return lexDiv, fileName


def cue_dict_html(trainingCase, devTestCase, n=3, upper=0.3, lower=0.1, dispersion=1, bonusWeight=1, stigmaWeight=1,
                  js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv"):
    """Modularised cueDict HTML files for use in other Python files.

    >>> cueDict, fileName = cue_dict_html([0], [1])
    >>> cueDict # doctest: +ELLIPSIS
    {1: ['The latter shows a spectrum of sarcoma, ...', '...', '...']}
    >>> fileName
    'cueDict'

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    upper: float, optional
        Threshold for determining bonus words; between [0, 1].

    lower: float, optional
        Threshold for determining stigma words; between [0, 1].

    dispersion: float, optional
        Dispersion threshold for determining null words; positive float.

    bonusWeight: float, optional
        The weight to be used for bonus words; typically positive int.

    stigmaWeight: float, optional
        The weight to be used for stigma words; typically positive int. Sign is flipped.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    Returns
    -------
    cueDict: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'cueDict'

    cueDict = cueDictionary.summarise(trainingCase, devTestCase, n, upper, lower, dispersion,
                                      bonusWeight, stigmaWeight, js, cs)

    generate_html(cueDict, fileName)

    return cueDict, fileName


def lin_reg_html(trainingCase, devTestCase, n=3, js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234):
    """Modularised linReg HTML files for use in other Python files.

    >>> linReg, fileName = lin_reg_html([0], [1])
    >>> linReg # doctest: +ELLIPSIS
    {1: ['It therefore appears that the LFS ...', '...', '...']}
    >>> fileName
    'linReg'

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    linReg: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'linReg'

    linReg = linearRegression.summarise(trainingCase, devTestCase, n, js, cs, seed)

    generate_html(linReg, fileName)

    return linReg, fileName


def ridge_cv_html(trainingCase, devTestCase, n=3, alphas=(0, 1, 10), numCV=10, js="BioASQ-trainingDataset4b.json",
                  cs="rouge_4b.csv", seed=1234):
    """Modularised ridgeCV HTML files for use in other Python files.

    >>> ridgeCV, fileName = ridge_cv_html([0], [1])
    >>> ridgeCV # doctest: +ELLIPSIS
    {1: ['It therefore appears that the LFS ...', '...', '...']}
    >>> fileName
    'ridgeCV'

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    alphas: List[int], optional
        List of alphas to be tested.

    numCV: int, optional
        Number of folds used for cross validation.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    ridgeCV: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'ridgeCV'

    ridgeCV = ridgeRegressionCV.summarise(trainingCase, devTestCase, n, alphas, numCV, js, cs, seed)

    generate_html(ridgeCV, fileName)

    return ridgeCV, fileName


def max_mar_rel_html(trainingCase, devTestCase, n=3, alpha=0.7, ngram=(1, 2), js="BioASQ-trainingDataset4b.json",
                     cs="rouge_4b.csv", seed=1234):
    """Modularised maxMarRel HTML files for use in other Python files.

    >>> maxMarRel, fileName = max_mar_rel_html([0], [1])
    >>> maxMarRel # doctest: +ELLIPSIS
    {1: ['It therefore appears that the LFS ...', '...', '...']}
    >>> fileName
    'maxMarRel'

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    alpha: float, optional
        Lambda value between [0, 1].

    ngram: Tuple(int, int)
        Tuple of lower and upper boundaries for range of n-values to be used in n-gram extraction.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    maxMarRel: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'maxMarRel'

    maxMarRel = maximalMarginalRelevance.summarise(trainingCase, devTestCase, n, alpha, ngram, js, cs, seed)

    generate_html(maxMarRel, fileName)

    return maxMarRel, fileName


def sup_vec_reg_html(trainingCase, devTestCase, n=3, kernel='rbf', js="BioASQ-trainingDataset4b.json",
                     cs="rouge_4b.csv", seed=1234):
    """Modularised supVecReg HTML files for use in other Python files.

    >>> supVecReg, fileName = sup_vec_reg_html([0], [1])
    >>> supVecReg # doctest: +ELLIPSIS
    {1: ['In addition, there seem to be ...', '...', '...']}
    >>> fileName
    'supVecReg'

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    kernel: string, optional
        Type of kernel to be used for learning the data sets.
        Can be 'linear', 'poly', 'rbf', 'sigmoid', 'precomputed' or a callable.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    supVecReg: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'supVecReg'

    supVecReg = supportVectorMachine.summarise(trainingCase, devTestCase, n, kernel, js, cs, seed)

    generate_html(supVecReg, fileName)

    return supVecReg, fileName


def oracle_html(devTestCase, n=3, cs="rouge_4b.csv"):
    """Modularised oracle HTML files for use in other Python files.

    >>> oracle, fileName = oracle_html([0])
    >>> oracle # doctest: +ELLIPSIS
    {0: ['Extracranial findings can include: ...', '...', '...']}
    >>> fileName
    'oracle'

    Parameters
    ----------
    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    Returns
    -------
    oracle: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'oracle'

    with open(cs, encoding='utf-8') as f:
        scores = [row for row in csv.reader(f)]

    header = scores[0]
    qIdIndex = header.index('qid')
    SU4Index = header.index('SU4')
    sentIndex = header.index('sentence text')

    idTopNTuple = []
    for qid in devTestCase:
        rows = []
        for row in scores[1:]:
            if qid == int(row[qIdIndex]):
                sentAndSU4 = (row[SU4Index], row[sentIndex])
                rows.append(sentAndSU4)
        rows.sort(key=lambda tup: tup[0], reverse=True)
        topNSU4 = rows[:n]
        topNSent = [sent[1] for sent in topNSU4]
        idTopNTuple.append((qid, topNSent))
    oracle = dict(idTopNTuple)

    generate_html(oracle, fileName)

    return oracle, fileName


def model_html(devTestCase, js="BioASQ-trainingDataset4b.json"):
    """Modularised model HTML files for use in other Python files.

    >>> model, fileName = model_html([0])
    >>> model # doctest: +ELLIPSIS
    {0: ['Muenke syndrome is characterized ...']}
    >>> fileName
    'model'

    Parameters
    ----------
    devTestCase: List[int]
        List of question IDs to be used for testing.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    Returns
    -------
    model: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    fileName: string
        Name of the file being generated.
    """
    fileName = 'model'

    with open(js) as f:
        data = json.load(f)
    questions = data['questions']

    idModelTuple = [(qid, questions[qid]['ideal_answer']) for qid in devTestCase]
    model = dict(idModelTuple)

    generate_html(model, fileName, modelFile=True)

    return model, fileName


def generate_html(dictObject, prefix, modelFile=False):
    """Write each system item's string into a HTML file.
    Requires a 'systems' and 'models' folder to already exist for use in ROUGE evaluation.

    Parameters
    ----------
    dictObject: Dict{int: List[string]}
        Dictionary containing question ID and their respective sentences.

    prefix: string
        The file name for the generated HTML file.

    modelFile: boolean, optional
        Whether the HTML generated is a model file.
    """
    if modelFile:
        for fileId, idealList in dictObject.items():
            if isinstance(idealList, list):
                for idx, sentences in enumerate(idealList):
                    text = _html_template(sentences)
                    fileLocation = 'models/' + str(fileId + 1) + '_' + str(idx + 1) + '.html'
                    _str_to_file(text, fileLocation)
            else:
                text = _html_template(idealList)
                fileLocation = 'models/' + str(fileId + 1) + '_1' + '.html'
                _str_to_file(text, fileLocation)
    else:
        for fileId, sentences in dictObject.items():
            text = _html_template(sentences)
            fileLocation = 'systems/' + prefix + '_' + str(fileId + 1) + '.html'
            _str_to_file(text, fileLocation)


def generate_xml(devTestCase, peers, models):
    """Write each system and model item's string into a XML file.

    Parameters
    ----------
    devTestCase: List[int]
        List of question IDs to be tested.

    peers: List[string]
        List of file names.

    models: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).
    """
    text = _xml_template(devTestCase, peers, models)
    _str_to_file(text, 'settings.xml')


########################################################################################################################
#                                              Private Helper Functions
########################################################################################################################


def _html_template(sentences):
    """Creates a HTML plaintext file string.

    >>> print(_html_template("Hello Nancy. Goodbye Nancy."))
    Hello Nancy.
    Goodbye Nancy.
    <BLANKLINE>
    >>> print(_html_template(["Hello Nancy. Goodbye Nancy.", "Hello and goodbye."]))
    Hello Nancy.
    Goodbye Nancy.
    Hello and goodbye.
    <BLANKLINE>

    Parameters
    ----------
    sentences: string or List[string]
        The sentence or sentences to be used.

    Returns
    -------
    htmlString: string
        String of passed sentences with newline between each.
    """
    htmlString = ''
    if isinstance(sentences, list):
        for sentence in sentences:
            sentList = sent_tokenize(sentence)
            for sent in sentList:
                htmlString += sent + '\n'
    else:
        sentList = sent_tokenize(sentences)
        for sent in sentList:
            htmlString += sent + '\n'
    return htmlString


def _xml_template(tasks, totalPeers, model):
    """Creates a XML file string.

    >>> print(_xml_template([1], ["hello"], {1: ["hello"]}))
    <?xml version="1.0" ?>
    <ROUGE-EVAL version="1.0">
        <EVAL ID="2">
            <PEER-ROOT>systems</PEER-ROOT>
            <MODEL-ROOT>models</MODEL-ROOT>
            <INPUT-FORMAT TYPE="SPL"/>
            <PEERS>
                <P ID="hello">hello_2.html</P>
            </PEERS>
            <MODELS>
                <M ID="1">2_1.html</M>
            </MODELS>
        </EVAL>
    </ROUGE-EVAL>
    <BLANKLINE>

    Parameters
    ----------
    tasks: List[int]
        List of question IDs to be used.

    totalPeers: List[string]
        List of file names.

    model: Dict{int: string or List[string]}
        Dictionary of question ID and their sentence(s).

    Returns
    -------
    xmlString: string
        A string of XML code with with prettified format.
    """
    peersHtml = []
    for qid in tasks:
        peerFiles = [peer + '_' + str(qid + 1) + '.html' for peer in totalPeers]
        peersHtml.append((qid, peerFiles))

    totalModels = []
    for fileId, idealList in model.items():
        if isinstance(idealList, list):
            modelFiles = [str(fileId + 1) + '_' + str(idx + 1) + '.html' for idx, sentences in enumerate(idealList)]
        else:
            modelFiles = [str(fileId + 1) + '_1' + '.html']
        totalModels.append((fileId, modelFiles))

    root = Element('ROUGE-EVAL', {'version': '1.0'})
    for qid in tasks:
        evalObject = SubElement(root, 'EVAL', {'ID': str(qid + 1)})

        peerRoot = SubElement(evalObject, 'PEER-ROOT')
        peerRoot.text = 'systems'

        modelRoot = SubElement(evalObject, 'MODEL-ROOT')
        modelRoot.text = 'models'

        inputFormat = SubElement(evalObject, 'INPUT-FORMAT', {'TYPE': 'SPL'})
        inputFormat.text = ''

        peers = SubElement(evalObject, 'PEERS')
        for peerId, peerFiles in peersHtml:
            if qid == peerId:
                for file in peerFiles:
                    p = SubElement(peers, 'P', {'ID': file.split('_')[0]})
                    p.text = file
                break

        models = SubElement(evalObject, 'MODELS')
        for modelId, modelFiles in totalModels:
            if qid == modelId:
                for file in modelFiles:
                    m = SubElement(models, 'M', {'ID': file[-6:-5]})
                    m.text = file
                break
    return _prettify(root)


def _str_to_file(text, fileName):
    """Write a file with the given name and the given text.

    Parameters
    ----------
    text: string
        The text to be written into the file.

    fileName: string
        The file's name.
    """
    with open(fileName, 'wb') as f:
        f.write(text.encode('utf-8'))


def _prettify(elem):
    """Return a pretty-printed XML string for the Element.

    >>> print(_prettify(Element('ROUGE-EVAL', {'version': '1.0'})))
    <?xml version="1.0" ?>
    <ROUGE-EVAL version="1.0"/>
    <BLANKLINE>

    Parameters
    ----------
    elem: Element
        XML Element object to be prettified.

    Returns
    -------
    prettyXml: string
        String of XML code with appropriate indentation.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent='    ')


if __name__ == "__main__":
    import doctest
    doctest.testmod()
    evaluate_all()
    print("ROUGE data saved in settings.xml. To run rouge, type e.g:")
    print("perl ~/share/ROUGE/RELEASE-1.5.5/ROUGE-1.5.5.pl -e ~/share/ROUGE/RELEASE-1.5.5/data -a -n 2 -2 4 -U settings.xml")
    
