"""
Maximal Marginal Relevance

Implementation of Maximal Marginal Relevance (MMR), based on Carbonell
and Goldstein (1998). MMR is implemented without query and multiple documents
in mind. Instead, this implementation focuses on sentences, by removing redundant
documents. It uses cosine similarity scoring for the sentences.
For more information on MMR, the paper can be found here:
<http://www.cs.cmu.edu/~jgc/publication/The_Use_MMR_Diversity_Based_LTMIR_1998.pdf>

Author: Richard Rui Gao <richard.gao@students.mq.edu.au>
Mentor: Diego Molla-Aliod <diego.molla-aliod@mq.edu.au>
"""

from sklearn import feature_extraction

import subprocess
import re

import numpy as np
import json
import random

from nltk import word_tokenize, sent_tokenize

import evaluation
import linearRegression


########################################################################################################################
#                                                 Main Functions
########################################################################################################################


def summarise(trainingCase, devTestCase, n=3, alpha=0.7, ngram=(1, 2), js="BioASQ-trainingDataset4b.json",
              cs="rouge_4b.csv", seed=1234):
    """Summarise a list of questions by Maximal Marginal Relevance.

    >>> summarise([0], [1]) # doctest: +ELLIPSIS
    {1: ['It therefore appears that the LFS phenotype ...', '...', '...']}

    Parameters
    ----------
    trainingCase: List[int]
        List of question IDs to be used for training.

    devTestCase: List[int]
        List of question IDs to be used for testing.

    n: int, optional
        Number of sentences to be used for each question ID.

    alpha: float, optional
        Lambda value between [0, 1].

    ngram: Tuple(int, int)
        Tuple of lower and upper boundaries for range of n-values to be used in n-gram extraction.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    Returns
    -------
    summary: Dict{int: string or List[string]}
        Summary of each tested question ID.
    """
    # Load json file for questionids
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))

    lr, cv = linearRegression.train_lr(trainingCase, cs, seed)

    # Create array of sentences from snippets per question
    snippetsPerId = []
    for qid in questionids:
        totalSnippets = []
        q = questions[int(qid)]
        snippets = q['snippets']
        for snippet in snippets:
            sentences = sent_tokenize(snippet['text'])
            for sentence in sentences:
                token = word_tokenize(sentence)
                alnumSent = [word for word in token if word.isalnum()]
                if alnumSent:
                    totalSnippets.append(sentence)
        snippetsPerId.append(totalSnippets)

    # Summarise question/snippets
    topNList = []
    for qid in devTestCase:
        idSents = snippetsPerId[qid]
        topNSents = []
        for steps in range(n):
            sentLeft = set(idSents) - set(topNSents)

            if not sentLeft:
                break

            bestSent = max(sentLeft, key=lambda x: _max_mar_rel(x, topNSents, lr, cv, alpha, ngram))
            topNSents.append(bestSent)
        topNList.append(topNSents)
    summary = dict(zip(devTestCase, topNList))

    return summary


def compare_cue_rouge(n=3, alpha=0.7, ngram=(1, 2), upper=0.3, lower=0.1, disp=1, bonus=1, stigma=1,
                  js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234, rougePath=".."):
    """Compare weight of words in maxMarRel to linReg and cue dictionary words.

    >>> compare_cue_rouge() # doctest: +SKIP
    Cue Dictionary F-score: 0.16962
    Linear Regression CV F-score: 0.15065
    Maximal Marginal Relevance F-score: 0.16447

    Parameters
    ----------
    n: int, optional
        Number of sentences to be used for each question ID.

    alpha: float, optional
        Lambda value between [0, 1].

    ngram: Tuple(int, int)
        Tuple of lower and upper boundaries for range of n-values to be used in n-gram extraction.

    upper: float, optional
        Threshold for determining bonus words; between [0, 1].

    lower: float, optional
        Threshold for determining stigma words; between [0, 1].

    disp: float, optional
        Dispersion threshold for determining null words; positive int.

    bonus: float, optional
        The weight to be used for bonus words; typically positive int.

    stigma: float, optional
        The weight to be used for stigma words; typically positive int. Sign is flipped.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising

    rougePath: string, optional
        Path where ROUGE is located.
    """
    # Open json file for questionids
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    # Create training and devTest cases
    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.8))
    devTestCase = [qid for qid in questionids if qid not in trainingCase]

    # Initialise maxMarRel, linReg, and cueDict
    peers = [evaluation.cue_dict_html(trainingCase, devTestCase, n, upper, lower, disp, bonus, stigma, js, cs)[1],
             evaluation.lin_reg_html(trainingCase, devTestCase, n, js, cs, seed)[1],
             evaluation.max_mar_rel_html(trainingCase, devTestCase, n, alpha, ngram, js, cs, seed)[1]]

    models = evaluation.model_html(devTestCase, js)[0]

    evaluation.generate_xml(devTestCase, peers, models)

    # Calculate ROUGE scores and read output
    with subprocess.Popen("perl " + rougePath + "/ROUGE-1.5.5.pl -e " + rougePath + "/data -a -x -2 4 -u settings.xml",
                          shell=True, stdout=subprocess.PIPE, universal_newlines=True).stdout as stream:
        lines = stream.read()
    cueDictScore = float(re.search("(?<=cueDict\sROUGE-SU4\sAverage_F:\s)(\d.\d{5})", lines).group(0))
    linRegScore = float(re.search("(?<=linReg\sROUGE-SU4\sAverage_F:\s)(\d.\d{5})", lines).group(0))
    maxMarRelScore = float(re.search("(?<=maxMarRel\sROUGE-SU4\sAverage_F:\s)(\d.\d{5})", lines).group(0))

    # Print ROUGE average F-scores
    print('Cue Dictionary F-score: {0}'.format(cueDictScore))
    print('Linear Regression CV F-score: {0}'.format(linRegScore))
    print('Maximal Marginal Relevance F-score: {0}'.format(maxMarRelScore))


########################################################################################################################
#                                                Helper Functions
########################################################################################################################


def find_alpha(noIter=11, n=3, ngram=(1, 2), js="BioASQ-trainingDataset4b.json", cs="rouge_4b.csv", seed=1234,
               rougePath=".."):
    """Find's the optimal alpha/lambda for MMR.

    >>> find_alpha() # doctest: +SKIP +ELLIPSIS
    ----------------------------
    9.09% done
    Best ROUGE score: 0.16729
    Current ROUGE score: 0.16729
    Best alpha: 0.0
    Current alpha: 0.0
    ....

    Parameters
    ----------
    noIter: string, optional
        Location of ROUGE folder.

    n: int, optional
        Number of sentences to be used for each question ID.

    ngram: Tuple(int, int)
        Tuple of lower and upper boundaries for range of n-values to be used in n-gram extraction.

    js: string, optional
        JSON file to be used for finding question IDs and sentences.

    cs: string, optional
        CSV file to be used for ROUGE SU4 scores.

    seed: any, optional
        Seed to be used for randomising.

    rougePath: string, optional
        Path where ROUGE is located.
    """
    with open(js) as f:
        data = json.load(f)
    questions = data['questions']
    # Simplify list of questions as questionids.
    questionids = list(range(len(questions)))
    # Remove questions with no snippets.
    for qid in questionids:
        q = questions[int(qid)]
        snippets = q['snippets']
        if not snippets:
            questionids.remove(qid)

    random.seed(seed)
    trainingCase = random.sample(questionids, int(len(questions) * 0.8))
    devTestCase = [qid for qid in questionids if qid not in trainingCase]

    threshold = np.linspace(0, 1, noIter)
    bestScore = 0.
    bestAlpha = 0
    count = 0

    peers = [evaluation.max_mar_rel_html(trainingCase, devTestCase, n, 0, ngram, js, cs, seed)[1]]
    models = evaluation.model_html(devTestCase, js)[0]
    evaluation.generate_xml(devTestCase, peers, models)

    for alpha in threshold:
        evaluation.max_mar_rel_html(trainingCase, devTestCase, n, alpha, ngram, js, cs, seed)

        with subprocess.Popen("perl " + rougePath + "/ROUGE-1.5.5.pl -e " + rougePath +
                              "/data -x -2 4 -u settings.xml maxMarRel", shell=True, stdout=subprocess.PIPE,
                              universal_newlines=True).stdout as stream:
            lines = stream.read()
        # Regex to find ROUGE f-score
        newScore = float(re.search("(?<=F:\s)(\d.\d{5})", lines).group(0))

        if bestScore < newScore:
            bestScore = newScore
            bestAlpha = alpha
        count += 1

        print('----------------------------')
        print('{0:.2f}% done'.format((count / noIter) * 100))
        print('Best ROUGE score:', bestScore)
        print('Current ROUGE score:', newScore)
        print('Best alpha:', bestAlpha)
        print('Current alpha:', alpha)
    print('----------------------------')
    print('Final alpha:', bestAlpha)


########################################################################################################################
#                                              Private Helper Functions
########################################################################################################################


def _max_mar_rel(xi, topNSents, lr, cv, lambda_=0.7, ngram=(1, 2)):
    """Maximum Marginal Relevance scoring module.

    >>> lr, cv = linearRegression.train_lr([0])
    >>> _max_mar_rel('Hello and goodbye.', [], lr, cv, 0.7, (1,2))
    0.091654222839907457

    Parameters
    ----------
    xi: string
        The sentence to be used for calculating.

    topNSents: List[string]
        Best N sentences specified; fills up with each iteration.

    lr: LinearRegression
        A fitted and learned LinearRegression function.

    cv: CountVectorizer
        A fitted CountVectorizer function.

    lambda_: float, optional
        Lambda value between [0, 1].

    ngram: Tuple(int, int), optional
        Tuple of lower and upper boundaries for range of n-values to be used in n-gram extraction.

    Returns
    -------
    mmrScore: float
        Score given from maxMarRel formula.
    """
    currentScore = _lr_score(xi, lr, cv)
    maxScore = max([_get_cosine(xi, xj, ngram) for xj in set(topNSents) - {xi}] or [0])
    return lambda_ * currentScore - (1 - lambda_) * maxScore


def _get_cosine(sent1, sent2, ngram=(1, 2)):
    """Get the cosine similarity of two vectors.
    Formula: AB/(|A||B|), for vectors A, B.

    >>> _get_cosine('Hello and goodbye.', 'Muenke syndrome goodbye.', (1,2))
    0.19999999999999996

    Parameters
    ----------
    sent1: string
        First sentence to be compared to.

    sent2: string
        Second sentence to be compared to.

    ngram: Tuple(int, int), optional
        Tuple of lower and upper boundaries for range of n-values to be used in n-gram extraction.

    Returns
    -------
    cosine: float
        Score given from cosine similarity formula.
    """
    cv = feature_extraction.text.CountVectorizer(token_pattern='\\b\\w+\\b', ngram_range=ngram)

    # Convert sentences into vectors with word counts.
    sentList = [sent1, sent2]
    sentMatrix = cv.fit_transform(sentList)
    vect1, vect2 = sentMatrix.toarray()

    # Use the cosine similarity formula.
    AB = np.dot(vect1, vect2)

    normA = np.linalg.norm(vect1)
    normB = np.linalg.norm(vect2)

    if normA == 0 or normB == 0:
        return 0

    return AB / (normA * normB)


def _lr_score(sent, lr, cv):
    """Get the weight of a sentence from linearRegression.py.

    >>> lr, cv = linearRegression.train_lr([0])
    >>> _lr_score('Muenke syndrome and goodbye.', lr, cv)
    0.11721210900428547

    Parameters
    ----------
    sent: string
        Sentence to be used to get predicted SU4 score.

    lr: LinearRegression
        A fitted and learned LinearRegression function.

    cv: CountVectorizer
        A fitted CountVectorizer function.

    Returns
    -------
    score: float
        Predicted SU4 score from linear regression.
    """
    sentTest = cv.transform([sent])
    return lr.predict(sentTest)[0]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
