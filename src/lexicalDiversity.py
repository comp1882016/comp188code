"""
Lexical Diversity

Calculates the lexical diversity of a given string.
Can summarise a given list of strings by finding the top 'n'
sentences with highest lexical diversity.

Author: Richard Rui Gao <richard.gao@students.mq.edu.au>
Mentor: Diego Molla-Aliod <diego.molla-aliod@mq.edu.au>
"""

from nltk import word_tokenize


########################################################################################################################
#                                                 Main Functions
########################################################################################################################


def summarise(sentences, n=10):
    """Return the top n sentences (list of words) of the given collection of lists of words.

    >>> sentences = ['Emma was here', "Emma wasn't here", 'Hello']
    >>> summarise(sentences, n=3)
    [('Emma was here', 1.0), ("Emma wasn't here", 1.0), ('Hello', 1.0)]

    Parameters
    ----------
    sentences: List[string]
        A list of sentences to be summarised.

    n: int, optional
        Specifies the number of sentences to be returned.

    Returns
    -------
    sortedList: List[(string, float)]
        A sorted list of highest lexical diversity sentences with lexical diversity score.
    """
    # Get top n list of sentences from highest to lowest based on lexical diversity score
    sortedNSent = sorted(sentences, key=lambda x: _lexical_diversity(x), reverse=True)[:n]
    # Get lexical diversity score for each sortedNSent
    sortedNScore = [_lexical_diversity(sent) for sent in sortedNSent]
    # Combine sortedNSent and sortedNScore into tuples
    sortedList = zip(sortedNSent, sortedNScore)
    return list(sortedList)


########################################################################################################################
#                                              Private Helper Functions
########################################################################################################################


def _lexical_diversity(sent):
    """Return the lexical diversity of a sentence.

    >>> words = "this test is a good test"
    >>> round(_lexical_diversity(words),3)
    0.833

    Parameters
    ----------
    sent: string
        The sentence to calculate lexical diversity.

    Returns
    -------
    score: float
        Lexical diversity score.
    """
    # Tokenize the string and ensure all tokens are alphanumeric
    tokens = [word.lower() for word in word_tokenize(sent) if word.isalnum()]
    # Return 0 if amount of total number of tokens is 0
    if len(tokens) == 0:
        return 0
    # Otherwise return number of unique tokens over total number of tokens
    return len(set(tokens)) / len(tokens)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
