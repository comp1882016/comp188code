# COMP188 Extractive Text Summarisation Code

-----------------------------------------------

## Authors

* **Diego Molla-Aliod** - *Mentor* - [diegoma](https://bitbucket.org/diegoma/)
* **Richard Gao** - *Student* - [LazyMilk](https://bitbucket.org/LazyMilk/)

## Acknowledgments

* **Department of Computing | Macquarie University**

-----------------------------------------------

This repository is used for the COMP188 course of Semester 1, 2016. Under the supervision of Dr. Diego Molla-Aliod, this repository holds all the code written during the course. The weekly journals and final report are available in the public wiki and should detail the process and analysis of results during the experiments done for the course. 

The code is all based on the research and development of automatic extractive text summarisation techniques and serves as a brief introduction into basic machine learning techniques. The results and code should prove useful for a comparison of older statistical-based approaches against more recent machine learning processes.

More information on the coding process and experiment results can be found in the wiki hosted on the BitBucket website [here](https://bitbucket.org/comp1882016/comp188code/wiki/Home).

*N.B. All the code, except lexicalDiversity.py, relies on a JSON and CSV file, and so it should be relatively troublesome to port for other datasets. The dependence is added due to specific guidelines in implementing the modules, unfortunately. The JSON and CSV files are provided in the repository as "BioASQ-trainingDataset4b.json" and "rouge_4b.csv" respectively, if you wish to test and use the code. The JSON file is a dataset file provided from the [BioASQ challenge](http://www.bioasq.org/) and the CSV file contains the [ROUGE evaluated](http://www.berouge.com/Pages/default.aspx) scores of every possible sentence from the JSON file.*

-----------------------------------------------

### Prerequisites

* Python 3.x
    * NumPy
    * SciPy
    * NLTK
    * scikit-learn
	* matplotlib

__Anaconda or other Python 3 distributions are recommended as all packages can be easily installed.__

* ROUGE - Recall-Oriented Understudy for Gisting Evaluation
    * Get ROUGE from their [website](http://www.berouge.com/Pages/default.aspx)
	* Read their documentation on how to use ROUGE
	* Requires Perl programming language
	


### Built With

* Python 3.5.1 | Anaconda 2.5.0 dist.

-----------------------------------------------

## Modules:

#### UML-Based Diagram of Module Dependencies

![comp188_uml.png](https://bitbucket.org/repo/L4Gezq/images/1377225993-comp188_uml.png)

The arrow direction indicates the dependency (e.g. if node A points to B, then B depends on A). 

The green arrow indicates the dependency of evaluation's generate_xml() function. Each function implements generate_xml() in order to individually calculate the ROUGE scores from their summary.

The red arrow indicates the evaluation module's implementation of the summarise() function from each related module. All the modules that return a summary will be used in evaluation's evaluate_all() function in order to generate a summary from each module for comparison.

The grey arrow with the closed triangle head indicates maximalMarginalRelevance's dependence on linearRegression's train_lr() function. The maximalMarginalRelevance module uses an algorithm that is dependent on a learnt predictive model, that being from linearRegression.

The grey arrow with the open head shows gridSearch calling to cueDictionary's summarise() function.

-----------------------------------------------

## Statistical Modules:

These modules are all based off of statistical approaches using the frequency and count of words in relation to their sentences or documents.

### Lexical Diversity - lexicalDiversity.py

Calculates the lexical diversity of a given string where the lexical diversity is the ratio of unique words to total words within a sentence/string.

Summarises a given list of strings by finding the top 'n' specified sentences with the highest lexical diversity.

Usage:

```python
>>> sentences = ['Emma was here', "Emma wasn't here", 'Hello']
>>> lexicalDiversity.summarise(sentences, n=3)
```

Output:

```python
[('Emma was here', 1.0), ("Emma wasn't here", 1.0), ('Hello', 1.0)]
```

### Cue Dictionary - cueDictionary.py

Based on [Edmundson (1969)], categorises the source text into bonus, stigma, and null words based on satisfaction of certain thresholds. Residue words are not implemented. More information on the specific implementation can be found in the [final report] and the [Edmundson (1969)] research paper.

Briefly, the approach assigns weights to words assigned to the three categories, where the words are assigned to their categories based on their ratio of word frequency in extractor-selected sentences to document frequency. This is implemented in the generate_cue() method.

Usage:

```python
>>> cues = generate_cue([0,1,2,3,4,5,6,7,8,9,10])
```

Output:

```python
>>> sorted(list(cues.keys()))
['bonus', 'null', 'stigma']

>>> sorted(cues['null'])
['actomyosin', 'administered', 'administration', 'after', 'also', ...

>>> print(len(cues['bonus']), len(cues['null']), len(cues['stigma']))
197 105 31
```

The summarise() method summarises a given list of question IDs by weighting all the sentences from each question ID, and finding the top 'n' specified sentences for each question.

Usage:

```python
>>> cueDictionary.summarise(trainingIds=[0,1], devTestIds=[2,3], n=3)
```

Output:

```python
{2: ['Alteplase can be given to patients ...', '...', '...'], 3: ['In addition ...', '...', '...']}
```

[Edmundson (1969)]: http://courses.ischool.berkeley.edu/i256/f06/papers/edmonson69.pdf
[final report]: https://bitbucket.org/comp1882016/comp188code/wiki/Final%20Report

#### Grid Search - gridSearch.py

The gridSearch module is only used in conjunction with the cueDictionary module. The gridSearch contains functions that will find the optimal thresholds or optimal weights for the cueDictionary module, based on the SU4 (skip four-gram) scores calculated from each summary. It brute force calculates the SU4 scores of all the possible thresholds between 0 to 1 or weights larger than 0, and retains the thresholds or weights that return the highest SU4 score.

Note, the weights are tested in absolute value such that bonus weights are positive and stigma weights are negative.

*This will take several minutes depending on the amount of iterations tested. More iterations will be more comprehensive and return a more accurate weight and threshold but will take an exponentially long time with time complexity O(n^2).*

Usage:

```python
>>> gridSearch.find_thresholds(lower=0, higher=1, noIter=10)
```

Output:

```python
----------------------------
2.22% done
Best ROUGE score: 0.1703
Current ROUGE score: 0.1703
Best thresholds: [0.10000000000000001, 0.0]
Current thresholds: [0.1, 0.0]
...
```

Usage:

```python
>>> gridSearch.find_weights(lower=1, higher=10, noIter=10)
```

Output:

```python
----------------------------
1.0 % done
Best ROUGE score: 0.16962
Current ROUGE score: 0.16962
Best weights: [1.0, 1.0]
Current weights: [1.0, 1.0]
...
```

-----------------------------------------------

## Machine Learning Modules:

These modules all use machine learning approaches using surface-level features. The feature used is the frequency of each word within each sentence and the training set. The feature is extracted using scikit-learn's CountVectorizer function.

### Linear Regression - linearRegression.py

Uses linear regression as a machine learning method, to learn the weights of each word from a source text (training set). 

The summarise() method will summarise a given list of question IDs by predicting the SU4 scores of each sentence from a question, and returning the top 'n' sentences for each question.

Usage:

```python
>>> linearRegression.summarise(trainingCase=[0], devTestCase=[1], n=3)
```

Output:

```python
{1: ['It therefore appears that the LFS phenotype ...', '...', '...']}
```

The compare_cue() method compares the weights assigned to the words from cueDictionary.generate_cue() to the weights in linearRegression. It will return a histogram of word weights from linearRegression that match the categories that the cue method has assigned them to. A statistical table will also be provided to show further information.

Usage:

```python
>>> linearRegression.compare_cue(n=3)
```

Output:

```python
----------------------------
Percent of words match their coefficients:

bonus: 52.980%
stigma: 49.520%
null: 2.184%
----------------------------
Number of words:

linearRegression | cueDict
bonus:      3222 | bonus:      3225
stigma:      937 | stigma:      947
null:       3480 | null:       3504
----------------------------
Possible weight thresholds?:

-5.286 >= bonus  >= 4.971
-2.007 >= stigma >= 3.035
-4.233 >= null   >= 3.427
----------------------------
Average weights:

bonus: 0.050
stigma: 0.035
null: 0.033
```

The compare_cue_rouge() method gives a quick comparison of linearRegression to cueDictionary by returning their respective ROUGE SU4 F-scores.

Usage:

```python
>>> linearRegression.compare_cue_rouge(n=3)
```

Output:

```python
Cue Dictionary F-score: 0.16962
Linear Regression F-score: 0.15065
```

Look through the source code for more thorough documentation of the methods as well as extra methods not specified here.

### Ridge Regression with Cross-Validation - ridgeRegressionCV.py

Like linear regression, but with regularisation and cross-validation, to reduce errors from learning a training set. Entirely based on scikit-learn's RidgeCV function. Higher cross-validation parameters will allow for better generalisation but will increase the time taken. A list of alphas is needed to test the different alphas and will return the optimal regression with the best alpha.

Includes find_alpha() to optimise the alpha separately to the summarise and fitting process. Alpha values are positive floats. The larger the value of the alpha, the smaller the values of the coefficients. Recommended process is to find the optimal alpha using find_alpha() and then use the optimal alpha as a single element list for the parameter in summarise(). *This will take several minutes depending on the size of the lists tested.*

Usage:

```python
>>> ridgeRegressionCV.find_alpha(alphas=[0, 1, 10], numCV=10)
```

Output:

```python
10
```

*This will take longer for higher values of numCV, that more cross validation k-folds.*

### Maximal Marginal Relevance - maximumMarginalRelevance.py

Implementation of Maximal Marginal Relevance (MMR), based on Carbonell and Goldstein (1998). MMR is implemented without query and multiple documents in mind. Instead, this implementation focuses on sentences, by removing redundant documents. It uses cosine similarity scoring for the sentences. For more information on MMR, the paper can be found [here]. The other scoring system is based on linear regression's predicted SU4 scores.

Includes find_alpha() to find the optimal alpha (otherwise lambda) used in the algorithm. The alpha value will be between 0 to 1 where an alpha of 1 is solely based on the linear regression score and an alpha of 0 is solely based on the cosine similarity score. *This will take several minutes depending on the amount of iterations used.*

Usage:

```python
>>> find_alpha(noIter=11, n=3)
```

Output:

```python
----------------------------
9.09% done
Best ROUGE score: 0.16729
Current ROUGE score: 0.16729
Best alpha: 0.0
Current alpha: 0.0
...
```

[here]: http://www.cs.cmu.edu/~jgc/publication/The_Use_MMR_Diversity_Based_LTMIR_1998.pdf

### Support Vector Machine - supportVectorMachine.py

Support Vector Machines (SVMs) are supervised learning models that represent data as categories within a space. New data are mapped within the space to become categorised. 

This implementation includes kernel support so that different kernels can be used for experimentation. They include 'linear', 'poly', 'rbf', 'sigmoid', and 'precomputed'.

Otherwise, the overall implementation, including summarise(), is similar to linear regression.

-----------------------------------------------

## Evaluation Tool - evaluation.py

Generates system and model files for use in ROUGE (see ROUGE documentation for more details). Can be run directly in command line without importing in python. It will create system and model files, for ROUGE score calculation, for all the python modules implemented here. In addition, randomly selected sentences and the CSV's (oracle) top 'n' sentences are used as summarisation systems and generate their own system files. All together, they can be used in comparison of the modules, with the oracle being the upper bound score and the random system being the lower bound score.

*Requires system and model folder to be created before use.*

Usage:

```python
>>> evaluation.evaluate_all(n=3)
```

*Will take a few minutes to generate all files, especially if using evaluate_all().*